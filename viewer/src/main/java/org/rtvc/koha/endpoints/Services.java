package org.rtvc.koha.endpoints;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;
import org.rtvc.koha.dao.PilotDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * Created by angelica on 12/28/15.
 */
@Controller
@RequestMapping("/")
public class Services {

    @Autowired
    private PilotDao pilotDao;

    private Logger logger = Logger.getLogger(this.getClass());

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    @ResponseBody
    public String getById(@PathVariable long id) {
        try {
            return getGson().toJson(pilotDao.getById(id));
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            return "";
        }
    }

    @RequestMapping(value = "/searchTitle/{term}", method = RequestMethod.GET)
    @ResponseBody
    public String searchByTitle(@PathVariable String term) {
        try {
            return getGson().toJson(pilotDao.searchTitle("%" + term + "%"));
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            return "";
        }
    }

    @RequestMapping(value = "/searchSynopsis/{term}", method = RequestMethod.GET)
    @ResponseBody
    public String searchBySynopsis(@PathVariable String term) {
        try {
            return getGson().toJson(pilotDao.searchSynopsis("%" + term + "%"));
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            return "";
        }
    }

    @RequestMapping(value = "/searchSeries/{term}", method = RequestMethod.GET)
    @ResponseBody
    public String searchBySeries(@PathVariable String term) {
        try {
            return getGson().toJson(pilotDao.searchSeries("%" + term + "%"));
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            return "";
        }
    }

    @RequestMapping(value = "/searchCast/{term}", method = RequestMethod.GET)
    @ResponseBody
    public String searchByCast(@PathVariable String term) {
        try {
            return getGson().toJson(pilotDao.searchCast("%" + term + "%"));
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            return "";
        }
    }

    @RequestMapping(value = "/searchSubjects/{term}", method = RequestMethod.GET)
    @ResponseBody
    public String searchBySubjects(@PathVariable String term) {
        try {
            return getGson().toJson(pilotDao.searchSubjects("%" + term + "%"));
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            return "";
        }
    }

    private Gson getGson() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create();
        return gson;
    }
}
