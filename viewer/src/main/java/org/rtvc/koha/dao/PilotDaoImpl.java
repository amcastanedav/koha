package org.rtvc.koha.dao;

import org.rtvc.koha.Pilot;
import org.rtvc.koha.mappers.PilotMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.util.List;

/**
 * Created by angelica on 12/28/15.
 */
public class PilotDaoImpl extends JdbcDaoSupport implements PilotDao {

    public final static String SELECT_BY_ID = "SELECT biblionumber, title, synopsis, url, series, cast, subjects FROM PILOT WHERE biblionumber LIKE ?";
    public final static String SEARCH_TITLE = "SELECT biblionumber, title, synopsis, url, series, cast, subjects FROM PILOT WHERE title LIKE ?";
    public final static String SEARCH_SYNOPSIS = "SELECT biblionumber, title, synopsis, url, series, cast, subjects FROM PILOT WHERE synopsis LIKE ?";
    public final static String SEARCH_SERIES = "SELECT biblionumber, title, synopsis, url, series, cast, subjects FROM PILOT WHERE series LIKE ?";
    public final static String SEARCH_CAST = "SELECT biblionumber, title, synopsis, url, series, cast, subjects FROM PILOT WHERE cast LIKE ?";
    public final static String SEARCH_SUBJECTS = "SELECT biblionumber, title, synopsis, url, series, cast, subjects FROM PILOT WHERE subjects LIKE ?";

    public Pilot getById(long id) throws Exception {
        return getJdbcTemplate().queryForObject(SELECT_BY_ID, new PilotMapper(), id);
    }

    public List<Pilot> searchTitle(String term) throws Exception {
        return getJdbcTemplate().query(SEARCH_TITLE, new PilotMapper(), term);
    }

    public List<Pilot> searchSynopsis(String term) throws Exception {
        return getJdbcTemplate().query(SEARCH_SYNOPSIS, new PilotMapper(), term);
    }

    public List<Pilot> searchSeries(String term) throws Exception {
        return getJdbcTemplate().query(SEARCH_SERIES, new PilotMapper(), term);
    }

    public List<Pilot> searchCast(String term) throws Exception {
        return getJdbcTemplate().query(SEARCH_CAST, new PilotMapper(), term);
    }

    public List<Pilot> searchSubjects(String term) throws Exception {
        return getJdbcTemplate().query(SEARCH_SUBJECTS, new PilotMapper(), term);
    }
}
