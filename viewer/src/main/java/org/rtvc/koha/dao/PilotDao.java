package org.rtvc.koha.dao;

import org.rtvc.koha.Pilot;

import java.util.List;

/**
 * Created by angelica on 12/28/15.
 */
public interface PilotDao {

    public Pilot getById(long id) throws Exception;

    public List<Pilot> searchTitle(String term) throws Exception;

    public List<Pilot> searchSynopsis(String term) throws Exception;

    public List<Pilot> searchSeries(String term) throws Exception;

    public List<Pilot> searchCast(String term) throws Exception;

    public List<Pilot> searchSubjects(String term) throws Exception;
}
