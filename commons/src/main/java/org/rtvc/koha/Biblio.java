package org.rtvc.koha;

import java.io.Serializable;

/**
 * Created by angelica on 12/21/15.
 */
public class Biblio implements Serializable {

    private long biblionumber;
    private String title;
    private String synopsis;

    public long getBiblionumber() {
        return biblionumber;
    }

    public void setBiblionumber(long biblionumber) {
        this.biblionumber = biblionumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    @Override
    public String toString() {
        return "Biblio{" +
                "biblionumber=" + biblionumber +
                ", title='" + title + '\'' +
                ", synopsis='" + synopsis + '\'' +
                '}';
    }
}
