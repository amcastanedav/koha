package org.rtvc.koha;

import java.io.Serializable;

/**
 * Created by angelica on 12/21/15.
 */
public class BiblioItem implements Serializable {

    private long biblioitemnumber;
    private String url;
    private String marcxml;

    public long getBiblioitemnumber() {
        return biblioitemnumber;
    }

    public void setBiblioitemnumber(long biblioitemnumber) {
        this.biblioitemnumber = biblioitemnumber;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMarcxml() {
        return marcxml;
    }

    public void setMarcxml(String marcxml) {
        this.marcxml = marcxml;
    }

    @Override
    public String toString() {
        return "BiblioItem{" +
                "biblioitemnumber=" + biblioitemnumber +
                ", url='" + url + '\'' +
                ", marcxml='" + marcxml + '\'' +
                '}';
    }
}
