package org.rtvc.koha;

import java.io.Serializable;

/**
 * Created by angelica on 12/21/15.
 */
public class Pilot implements Serializable {

    private long biblionumber;
    private String title;
    private String synopsis;
    private String url;
    private String series;
    private String cast;
    private String subjects;

    public long getBiblionumber() {
        return biblionumber;
    }

    public void setBiblionumber(long biblionumber) {
        this.biblionumber = biblionumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    @Override
    public String toString() {
        return "Pilot{" +
                "biblionumber=" + biblionumber +
                ", title='" + title + '\'' +
                ", synopsis='" + synopsis + '\'' +
                ", url='" + url + '\'' +
                ", series='" + series + '\'' +
                ", cast='" + cast + '\'' +
                ", subjects='" + subjects + '\'' +
                '}';
    }
}
