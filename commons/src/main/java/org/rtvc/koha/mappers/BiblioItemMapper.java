package org.rtvc.koha.mappers;

import org.rtvc.koha.BiblioItem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by angelica on 12/21/15.
 */
public class BiblioItemMapper implements RowMapper<BiblioItem> {

    public static final String biblionumber = "biblionumber";
    public static final String url = "url";
    public static final String marcxml = "marcxml";

    public BiblioItem mapRow(ResultSet resultSet, int i) throws SQLException {
        BiblioItem biblioItem = new BiblioItem();
        biblioItem.setBiblioitemnumber(resultSet.getLong(biblionumber));
        biblioItem.setUrl(resultSet.getString(url));
        biblioItem.setMarcxml(resultSet.getString(marcxml));
        return biblioItem;
    }
}
