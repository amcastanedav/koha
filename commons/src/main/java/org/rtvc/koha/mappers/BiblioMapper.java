package org.rtvc.koha.mappers;

import org.rtvc.koha.Biblio;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by angelica on 12/21/15.
 */
public class BiblioMapper implements RowMapper<Biblio> {

    public static final String biblionumber = "biblionumber";
    public static final String title = "title";
    public static final String synopsis = "abstract";

    public Biblio mapRow(ResultSet resultSet, int i) throws SQLException {
        Biblio biblio = new Biblio();
        biblio.setBiblionumber(resultSet.getLong(biblionumber));
        biblio.setTitle(resultSet.getString(title));
        biblio.setSynopsis(resultSet.getString(synopsis));
        return biblio;
    }
}
