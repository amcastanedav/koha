package org.rtvc.koha.mappers;

import org.rtvc.koha.Pilot;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by angelica on 12/28/15.
 */
public class PilotMapper implements RowMapper<Pilot> {

    public static final String biblionumber = "biblionumber";
    public static final String title = "title";
    public static final String synopsis = "synopsis";
    public static final String url = "url";
    public static final String series = "series";
    public static final String cast = "cast";
    public static final String subjects = "subjects";

    public Pilot mapRow(ResultSet resultSet, int i) throws SQLException {
        Pilot pilot = new Pilot();
        pilot.setBiblionumber(resultSet.getLong(biblionumber));
        pilot.setTitle(resultSet.getString(title));
        pilot.setSynopsis(resultSet.getString(synopsis));
        pilot.setUrl(resultSet.getString(url));
        pilot.setSeries(resultSet.getString(series));
        pilot.setCast(resultSet.getString(cast));
        pilot.setSubjects(resultSet.getString(subjects));
        return pilot;
    }
}
