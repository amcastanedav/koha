package org.rtvc.koha.model;

import org.apache.log4j.Logger;
import org.rtvc.koha.Biblio;
import org.rtvc.koha.BiblioItem;
import org.rtvc.koha.Pilot;
import org.rtvc.koha.dao.BiblioDao;
import org.rtvc.koha.dao.BiblioItemDao;
import org.rtvc.koha.dao.PilotDao;

import java.util.HashMap;
import java.util.List;

/**
 * Created by angelica on 12/23/15.
 */
public class Executor {

    private BiblioDao biblioDao;
    private BiblioItemDao biblioItemDao;
    private PilotDao pilotDao;
    private Migrator migrator;
    private int size;

    private Logger logger = Logger.getLogger(this.getClass());

    public void migrate(){
        logger.info("Migration start");

        try {
            pilotDao.clean();
            int biblioTotal = biblioDao.total();
            int items = 0;

            while (items < biblioTotal){
                logger.info("Items " + items);
                List<Biblio> biblioList = biblioDao.retrieve(size,items);
                items = items + size;

                long minor = biblioList.get(0).getBiblionumber();
                long mayor = biblioList.get(biblioList.size()-1).getBiblionumber();

                List<BiblioItem> biblioItemList = biblioItemDao.retrive(minor, mayor);
                HashMap<Long,BiblioItem> biblioItemHashMap = listToMapBiblioItem(biblioItemList);

                for (Biblio biblio:biblioList){
                    Pilot pilot = migrator.migrate(biblio,biblioItemHashMap.get(biblio.getBiblionumber()));
                    pilotDao.insert(pilot);
                }
            }


        } catch (Exception e) {
            logger.error("Something went wrong during migration");
            logger.error(e.getMessage());
        }
    }

    private HashMap<Long,BiblioItem> listToMapBiblioItem(List<BiblioItem> biblioItemList){
        HashMap<Long,BiblioItem> biblioItemHashMap = new HashMap<Long, BiblioItem>();

        for (BiblioItem biblioItem:biblioItemList)
            biblioItemHashMap.put(biblioItem.getBiblioitemnumber(),biblioItem);

        return biblioItemHashMap;
    }

    public void setBiblioDao(BiblioDao biblioDao) {
        this.biblioDao = biblioDao;
    }

    public void setBiblioItemDao(BiblioItemDao biblioItemDao) {
        this.biblioItemDao = biblioItemDao;
    }

    public void setPilotDao(PilotDao pilotDao) {
        this.pilotDao = pilotDao;
    }

    public void setMigrator(Migrator migrator) {
        this.migrator = migrator;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
