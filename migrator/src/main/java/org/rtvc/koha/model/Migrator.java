package org.rtvc.koha.model;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.marc4j.MarcReader;
import org.marc4j.MarcXmlReader;
import org.marc4j.marc.DataField;
import org.marc4j.marc.Record;
import org.rtvc.koha.Biblio;
import org.rtvc.koha.BiblioItem;
import org.rtvc.koha.Pilot;
import java.io.InputStream;
import java.util.List;

/**
 * Created by angelica on 12/23/15.
 */
public class Migrator {

    private Logger logger = Logger.getLogger(this.getClass());

    public Pilot migrate(Biblio biblio, BiblioItem biblioItem){
        Pilot pilot = new Pilot();

        if (!(biblio.getBiblionumber() == biblioItem.getBiblioitemnumber()))
            return pilot;

        pilot.setBiblionumber(biblio.getBiblionumber());
        pilot.setTitle(biblio.getTitle());
        pilot.setSynopsis(biblio.getSynopsis());
        pilot.setUrl(biblioItem.getUrl());

        String marxml = biblioItem.getMarcxml();

        try {
            InputStream in = IOUtils.toInputStream(marxml, "UTF-8");
            MarcReader reader = new MarcXmlReader(in);

            while (reader.hasNext()){
                Record record = reader.next();
                List<DataField> dataFields = record.getDataFields();
                for (DataField dataField:dataFields){
                    if (dataField.getTag().equalsIgnoreCase("490")){
                        pilot.setSeries(dataField.getSubfield('a').getData());
                    }
                    if (dataField.getTag().equalsIgnoreCase("511")){
                        pilot.setCast(dataField.getSubfield('a').getData());
                    }
                    if (dataField.getTag().equalsIgnoreCase("654")){
                        pilot.setSubjects(dataField.getSubfield('a').getData());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error parsing xml");
            logger.error(e.getClass());
            logger.error(e.getMessage());
        }

        return pilot;
    }
}
