package org.rtvc.koha.dao;

import org.rtvc.koha.Biblio;

import java.util.List;

/**
 * Created by angelica on 12/21/15.
 */
public interface BiblioDao {

    public int total() throws Exception;

    public Biblio retrieveById(long id) throws Exception;

    public List<Biblio> retrieve(int limit, int offset) throws Exception;

    public List<Biblio> retrieve(long start, long end) throws Exception;
}
