package org.rtvc.koha.dao;

import org.rtvc.koha.BiblioItem;

import java.util.List;

/**
 * Created by angelica on 12/21/15.
 */
public interface BiblioItemDao {

    public int total() throws Exception;

    public BiblioItem retrieveById(long id) throws Exception;

    public List<BiblioItem> retrive(int limit, int offset) throws Exception;

    public List<BiblioItem> retrive(long start, long end) throws Exception;
}
