package org.rtvc.koha.dao;

import org.rtvc.koha.Pilot;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

/**
 * Created by angelica on 12/21/15.
 */
public class PilotDaoImpl extends JdbcDaoSupport implements PilotDao {

    public final static String DELETE = "DELETE FROM PILOT";
    public final static String INSERT = "INSERT INTO PILOT (biblionumber, title, synopsis, " +
            "url, series, cast, subjects) VALUES (?,?,?,?,?,?,?)";

    public void clean() throws Exception {
        getJdbcTemplate().update(DELETE);
    }

    public int insert(Pilot pilot) throws Exception {
        return getJdbcTemplate().update(INSERT,
                pilot.getBiblionumber(),
                pilot.getTitle(),
                pilot.getSynopsis(),
                pilot.getUrl(),
                pilot.getSeries(),
                pilot.getCast(),
                pilot.getSubjects());
    }
}
