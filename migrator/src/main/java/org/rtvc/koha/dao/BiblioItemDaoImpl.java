package org.rtvc.koha.dao;

import org.rtvc.koha.BiblioItem;
import org.rtvc.koha.mappers.BiblioItemMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.util.List;

/**
 * Created by angelica on 12/21/15.
 */
public class BiblioItemDaoImpl extends JdbcDaoSupport implements BiblioItemDao {

    public final static String COUNT = "SELECT COUNT(*) FROM biblioitems";
    public final static String SELECT_BY_ID = "SELECT biblionumber, url, marcxml FROM biblioitems WHERE biblionumber LIKE ?";
    public final static String SELECT_OFFSET = "SELECT biblionumber, url, marcxml FROM biblioitems ORDER BY biblionumber ASC LIMIT ? OFFSET ?";
    public final static String SELECT_LIMIT = "SELECT biblionumber, url, marcxml FROM biblioitems WHERE biblionumber >= ? AND biblionumber <= ?";

    public int total() throws Exception {
        return getJdbcTemplate().queryForInt(COUNT);
    }

    public BiblioItem retrieveById(long id) throws Exception {
        return getJdbcTemplate().queryForObject(SELECT_BY_ID, new BiblioItemMapper(), id);
    }

    public List<BiblioItem> retrive(int limit, int offset) throws Exception {
        return getJdbcTemplate().query(SELECT_OFFSET, new BiblioItemMapper(), limit, offset);
    }

    public List<BiblioItem> retrive(long start, long end) throws Exception {
        return getJdbcTemplate().query(SELECT_LIMIT, new BiblioItemMapper(), start, end);
    }
}
