package org.rtvc.koha.dao;

import org.rtvc.koha.Biblio;
import org.rtvc.koha.mappers.BiblioMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import java.util.List;

/**
 * Created by angelica on 12/21/15.
 */
public class BiblioDaoImpl extends JdbcDaoSupport implements BiblioDao {

    public final static String COUNT = "SELECT COUNT(*) FROM biblio";
    public final static String SELECT_BY_ID = "SELECT biblionumber, title, abstract FROM biblio WHERE biblionumber LIKE ?";
    public final static String SELECT_OFFSET = "SELECT biblionumber, title, abstract FROM biblio ORDER BY biblionumber ASC LIMIT ? OFFSET ?";
    public final static String SELECT_LIMIT = "SELECT biblionumber, title, abstract FROM biblio WHERE biblionumber >= ? AND biblionumber <= ?";

    public int total() throws Exception {
        return getJdbcTemplate().queryForInt(COUNT);
    }

    public Biblio retrieveById(long id) throws Exception {
        return getJdbcTemplate().queryForObject(SELECT_BY_ID, new BiblioMapper(), id);
    }

    public List<Biblio> retrieve(int limit, int offset) throws Exception {
        return getJdbcTemplate().query(SELECT_OFFSET, new BiblioMapper(), limit, offset);
    }

    public List<Biblio> retrieve(long start, long end) throws Exception {
        return getJdbcTemplate().query(SELECT_LIMIT, new BiblioMapper(), start, end);
    }
}
