package org.rtvc.koha.dao;

import org.rtvc.koha.Pilot;

/**
 * Created by angelica on 12/21/15.
 */
public interface PilotDao {

    public void clean() throws Exception;

    public int insert(Pilot pilot) throws Exception;
}
