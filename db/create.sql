CREATE TABLE PILOT(
  biblionumber        BIGINT NOT NULL,
  title               MEDIUMTEXT,
  synopsis            MEDIUMTEXT,
  url                 MEDIUMTEXT,
  series              MEDIUMTEXT,
  cast                MEDIUMTEXT,
  subjects            MEDIUMTEXT
);